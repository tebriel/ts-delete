import os
import requests

API_KEY = os.getenv("TS_API_KEY")
BASE_URL = "https://api.tailscale.com/api/v2"

from requests.auth import HTTPBasicAuth

basic = HTTPBasicAuth(API_KEY, None)
resp = requests.get(f"{BASE_URL}/tailnet/-/devices", auth=basic)
resp.raise_for_status()
for device in resp.json()["devices"]:
    if device["hostname"] == "paste":
        print(f"{device['hostname']}: {device['id']}")
        resp = requests.delete(f"{BASE_URL}/device/{device['id']}", auth=basic)
        print(resp.status_code)
