{ pkgs, ... }:

{
  # https://devenv.sh/basics/
  env.GREET = "devenv";

  # https://devenv.sh/packages/
  packages = [ pkgs.git ];

  # https://devenv.sh/scripts/
  scripts.hello.exec = "echo hello from $GREET";

  enterShell = ''
    hello
    git --version
  '';

  # https://devenv.sh/languages/
  # languages.nix.enable = true;
  dotenv.enable = true;

  languages.python.enable = true;
  languages.python.version = "3.11.3";
  languages.python.poetry.enable = true;

  languages.python.poetry.activate.enable = true;
  languages.python.poetry.install.enable = true;

  # https://devenv.sh/pre-commit-hooks/
  pre-commit.hooks.black.enable = true;

  # https://devenv.sh/processes/
  # processes.ping.exec = "ping example.com";

  # See full reference at https://devenv.sh/reference/options/
}
